---
title: "Premier Billet Avec Hugo"
date: 2017-11-05T13:04:01+01:00
draft: true
---

# Mon premier billet avec Hugo

Hugo est un générateur de site statique tout comme Jekyll ou bien d'autres. Une fois le client installé et quelques commandes relativement simple, on parvient rapidement à faire son site.
## Installation

Pour l'installation d'Hugo sur Ubuntu, il est possible de le faire par différentes :

* via les packages Ubuntu : la version est très anciennes et n'est pas compatible avec le thème que j'ai choisi (Cactus) mais j'imagine qu'il en est de même pour les autres.

```
sudo apt install hugo
```

ici on a une version 0.16 :(

* via la nouvelle méthode de containérisation de package **snap** : 

```
sudo snap install hugo
```

ici on a la version 0.32 mais lorsqu'on lance le client on a le message suivant :

```
snap-confine has elevated permissions and is not confined but should be. Refusing to continue to avoid permission escalation attacks
```

Ce message semble être un bug connu, lié à la version 16.04 d'Ubuntu : voir [ici](https://bugs.launchpad.net/ubuntu/+source/snapd/+bug/1673247)

Pour ma première tentative avec Snap, cela aurait été un échec. Il parait que Flatpak c'est mieux mais je n'ai pas d'avis sur le sujet.

* à la main :

C'est la méthode qui a fonctionné pour moi. Je suis allé sur le Github de Hugo est j'ai télécharge le .deb et je l'ai installé avec `dpkg`

