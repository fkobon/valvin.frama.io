+++
author = "ValVin"
categories = ["linux"]
date = 2016-01-18T19:47:37Z
description = ""
draft = false
featured_image = "/content/images/2016/01/dance-108915_640.jpg"
slug = "spotify-beta-sur-linux"
tags = ["linux"]
title = "Spotify Bêta sur Linux"

+++

Dans le monde du streaming musical, il y a Spotify. Il propose une offre gratuite, mais forcément avec de la publicité suffisamment régulièrement pour te forcer à passer à l'offre Premium. 

Le but du billet n'est pas de leur faire de la pub mais de retranscrire une mini procédure permettant d'avoir le dernier client Linux bêta.

Au début, j'avais suivi [cette procédure "officielle"](https://www.spotify.com/fr/download/linux/) mais le client n'est pas spécialement à jour.

Et en discutant aujourd'hui, un collègue m'a fait découvrir une version bêta qui date de 06/2015 ... donc avant qu'elle devienne release, autant partager :

Ajout du repository testing de Spotify et installation :
```bash
echo deb http://repository.spotify.com testing non-free | sudo tee /etc/apt/sources.list.d/spotify.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 13B00F1FD2C19886
sudo apt-get update
```
Si le client précédent est déjà installé :
```bash
sudo apt-get upgrade
```
Sinon :
```
sudo apt-get install spotify-client 
```

[Source](https://community.spotify.com/t5/Spotify-Community-Blog/Spotify-Client-1-x-beta-for-Linux-has-been-released/ba-p/1147084)

Photo : [Pixabay - Danse musique la clef de sol](https://pixabay.com/fr/danse-la-musique-clef-de-sol-108915/)