+++
author = "ValVin"
categories = ["vie-privée", "degooglisons", "opensource"]
date = 2016-01-22T20:29:32Z
description = ""
draft = false
featured_image = "/content/images/2016/01/write-593333_640.jpg"
slug = "laverna-des-notes-privees-et-chiffrees"
tags = ["vie-privée", "degooglisons", "opensource"]
title = "Laverna des notes privées et chiffrées"

+++

Depuis plusieurs années, je cherche un outils de prise de notes. Mes critères principaux sont que celui-ci soit simple d'utilisation, puisse se synchroniser sur le cloud mais tout en étant respectueux de ma vie privée.

J'avoue que les puristes diront que j'ai flirté avec le mal car j'ai commencé avec **Evernote**, puis Microsoft **One Note** puis **[Zim](http://zim-wiki.org/)**. Ce dernier est le seul qui est respectueux de la vie privée mais on perd un gros avantage qui est la synchronisation des données sur le cloud. J'avais essayé **[TagSpaces](http://www.tagspaces.org/)** mais il me manquait quelques fonctionnalités essentielles qui sont peut être maintenant corrigées (recherche full text notammenet, uniquement via les tags). Evernote et OneNote avaient les fonctionnalités souhaitées mais pour le côté vie-privée, comment dire, c'est un peu comme laisser un billet de 50€ au sol et penser repasser quelques heures plus tard pour le récupérer.

J'ai récemment découvert **[Laverna](https://laverna.cc/)** et celui-ci répond à mes principaux critères. Bien qu'encore un peu jeune, je l'utilise désormais quotidiennement et je vous propose de vous le faire découvrir.

##Laverna des notes privées et chiffrées
Tout d'abord, il s'agit d'une application, bien entendu, **OpenSource** sous licence [MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/) dont les sources sont hébergées sur [GitHub](https://github.com/Laverna/laverna)
![](/content/images/2016/01/d51bffb8-laptop-edit.png)
Cet outil permet donc de prendre des notes au format **Markdown**. Pour ceux qui ne le connaissent pas il s'agit d'un langage permettant de se limiter à la mise en forme rudimentaire pour se concentrer sur le contenu.  
J'utilise d'ailleurs ce langage pour écrire ce billet (Ghost). On le retrouve régulièrement sur le web car ce langage est simple et se transforme très facilement en HTML. 

Laverna utilise [Pagedown](https://code.google.com/p/pagedown/) comme éditeur tout en ajoutant la coloration syntaxique. 
Il est également possible pour ceux qui en ont l'utilité d'écrire des formules mathématiques grâce à [Mathjax](https://www.mathjax.org/).  
Une fonctionnalité de gestion de tâches est possible. Pour chaque note, un pourcentage de la réalisation apparaît en entête de la note.

Laverna repose sur une application **HTML5** pour laquelle il n'est pas nécessaire de créer de compte. Les données sont stockées dans le **LocalStorage** du navigateur. Il n'y a pas de serveur d'application, tout se passe sur le navigateur. Techniquement, si vous utilisez la [démo](https://laverna.cc/app/) disponible sur le site de Laverna, aucune donnée ne sera stockée sur le serveur qui héberge https://laverna.cc.

Tout reste en local dans les données du navigateur :
![](/content/images/2016/01/Capture-du-2016-01-20-21-44-40.png)

Ces données peuvent être **chiffrées** à l'aide de la [Standford Javascript Crypto Library](http://bitwiseshiftleft.github.io/sjcl/) (SJCL) et propose de l'**AES** 128/192/256. Une fois le chiffrement activé, il suffit de saisir son mot de passe pour accéder à son contenu.
Cette fonctionnalité permet de contrôler l'accès aux données sur le poste sur lequel vous utilisez l'outil mais surtout de pouvoir **synchroniser vos notes** dans le cloud en toute sécurité.
![](/content/images/2016/01/encrypted-445155_640-resized.jpg)

La synchronisation peut se faire sur Dropbox ou tout service implémentant [RemoteStorage](https://remotestorage.io/). Pour ma part, j'utilise Dropbox, peut être que c'est/sera compatible [Framadrive](https://framadrive.org/) ?
![](/content/images/2016/01/cloud-158481_640-resized.png)

Ce qu'il faut savoir quand on utilise la synchronisation de notes c'est que les paramètres ne le sont pas. Pour des raisons évidentes de sécurité, on ne laisse pas les clés sur la porte, sinon autant ne rien chiffrer. Il faut donc exporter sa configuration pour l'utiliser sur un autre périphérique.

On se retrouve donc avec des notes synchroniser dans le cloud mais sans que personne d'autre que vous puisse y accéder.

L'application est disponible en version Desktop grâce à [Electron](http://electron.atom.io/). Si je fais un raccourci c'est un NodeWebkit à jour.

On peut donc avoir ses notes sur son PC, via l'application Electron ou un site web hébergeant la version statique de l'application. Cette dernière pourra être également utilisé sur son smartphone / tablette. La version PhoneGap n'est pas tout a fait fonctionelle.

**Mode visualisation :**
![](/content/images/2016/01/Capture-du-2016-01-22-21-59-45.png)

**Mode édition :**
![](/content/images/2016/01/Capture-du-2016-01-22-21-58-48.png)

Pour ce dernier, il est possible d'avoir plusieurs mode d'édition avec ou sans prévisualisation ainsi qu'un mode plein écran.

Si vous souhaitez l'utiliser, toutes les sources sont disponibles sur Github. Pour les plus fainéants (comme moi), on reccupérer la dernière release [ici](https://github.com/Laverna/laverna/releases). Pour les plus courageux, on peut compiler le bignou en suivant la procédure du README. Il faut juste savoir qu'il vaut mieux un nodejs à jour. Rien de tel qu'un :

```
sudo npm cache clean -f
sudo npm install -g n
sudo n stable
sudo ln -sf /usr/local/n/versions/node/<VERSION>/bin/node /usr/bin/node
```

Il est également possible de construire un APK PhoneGap avec la commande :

```
grunt platform-build
```
Malheureusement, j'avoue que le résultat est assez décevant. Je n'ai pas réussi à réccupérer mes notes qui sont synchronisées sur Dropbox.

Mais comme indiqué plus haut, il est possible de parcourir ces notes (avec quelques bugs d'affichage) avec son navigateur mobile. On peut apporter des modifications mineures, mais le mobile n'est clairement pas le support à privilégier ;-)

Le projet semble être assez actif et j'espère que cela donnera un bon produit final :) Une bon candidat pour **Framanotes** ? 

**Crédits photos** : 

- [Pixabay - StartupStockPhoto ](https://pixabay.com/fr/%C3%A9crire-plan-d-affaires-d%C3%A9marrage-593333/)
- [Pixabay - HebiFot](https://pixabay.com/fr/crypt%C3%A9-politique-de-confidentialit%C3%A9-445155/)
- [Pixabay - OpenClipartVector](https://pixabay.com/fr/nuage-informatique-avenir-internet-158481/)

