+++
author = "ValVin"
categories = ["vie-privée", "opensource"]
date = 2016-03-29T18:47:34Z
description = ""
draft = false
featured_image = "/content/images/2016/03/smssecure.png"
slug = "smssecure-menace-par-celltrust-pour-violation-de-marque"
tags = ["vie-privée", "opensource"]
title = "SMSSecure menacé par CellTrust pour violation de marque"

+++

**[SMSSecure](https://smssecure.org/)** est une application qui comme son nom l'indique permet de sécurisé les SMS/MMS sur nos smartphones. Contrairement à Signal (anciennement TextSecure), celle-ci ne fait pas intervenir de serveurs tiers.

Il est possible d'échanger des SMS / MMS normalement, ceux-ci sont stockés dans un "container sécurisé" et si l'un de vos correspondants utilise également cette applicaiton, l'échange du SMS se fait de façon totalement chiffré. Il s'agit d'une application est gratuite et 100% OpenSource et développé par [Bastien Le Querrec](https://twitter.com/BastienLQ) et [Carey Mecalfe](https://twitter.com/CareyMetcalfe)

J'ai déjà parlé de SMSSecure dans ce [billet](https://blog.valvin.fr/2015/11/05/android-sans-google-1/). J'en reparle aujourd'hui car je lis sur Diaspora et [Twitter](https://twitter.com/BastienLQ/status/714888755627311106) qu'une société s'appelant CellTrust menace SMSSecure car elle détient la marque SecureSMS.
![](/content/images/2016/03/Celtrust-SMSSecure.jpeg)
*[Source](https://twitter.com/BastienLQ/status/714888755627311106)*
 
Ce genre de comportement me dégoûte et quand on y regarde de plus près, je n'ai pas l'impression que cette société souhaite défendre sa marque, qui soit dit en passant n'est pas spécialement connue, mais plutôt son business.

Effectivement CellTrust propose des services aux entreprises autour de la sécurité des [échanges SMS](http://www.celltrust.com/products/celltrust-secureline/#messaging) / téléphoniques de leurs employés ...  Donc forcément apporter de la sécurité dans les échanges SMS et tout ça **gratuitement** et avec un **code ouvert**, ça n'arrange pas leur business. 
En plus ils vantent le mérite des campagnes ~~de spam~~ marketing par SMS ... bref, une société comme je les aime (ou pas)... 

Alors bon j'espère que dans ce genre de situation, cela reste sans suite et qu'au mieux cela génère un peu de "bad buzz" pour cette société qui essaie de justifier le budget alloué à son service juridique avec ce genre de pratique honteuse...

Et surtout merci à Bastien et Carey pour cette application d'utilité publique. Longue vie à SMSSecure, n'en déplaise à CellTrust ! :)