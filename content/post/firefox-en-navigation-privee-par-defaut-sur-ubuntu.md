+++
author = "ValVin"
categories = ["vie-privée", "firefox"]
date = 2015-11-11T20:07:23Z
description = ""
draft = false
featured_image = "/content/images/2015/11/firefox-private-mode.png"
slug = "firefox-en-navigation-privee-par-defaut-sur-ubuntu"
tags = ["vie-privée", "firefox"]
title = "Firefox en navigation privée par défaut sur Ubuntu"

+++

EDIT 18/11/15 : je pense que j'ai écrit ce billet pour rien ... il y a en fait une option dans les paramètres "Vie-Privée" ou l'on peut activer "Toujours utiliser le mode de navigation privée"
![](/content/images/2015/11/Capture-du-2015-11-18-22-55-31.png)

Dans les grandes nouveautés de [Firefox 42](https://www.mozilla.org/en-US/firefox/42.0/releasenotes/), il y a le nouveau mode de navigation privée. Celui-ci protège plus activement les données personnelles.

Utilisateur d'Ubuntu, je me disais que ce serait intéressant que lorsque je clique sur mon icône firefox, cela lance par défaut le mode de navigation privée. 

J'ai rapidement trouvé la ligne de commande permettant de lancer firefox en mode privée : `firefox -private`

Pour modifier l'icône du lanceur, Unity fonctionne avec des fichier `*.desktop` qui sont expliqués [ici](https://help.ubuntu.com/community/UnityLaunchersAndDesktopFiles). Ces fichiers se situent dans `/usr/share/applications`

Celui de Firefox est donc `/usr/share/applications/firefox.desktop`

Il suffit donc d'éditer ce fichier et modifier la ligne qui commence par `Exec=` de la section `Desktop Entry`. Je précise la section car l'icône contenant deux actions, une pour ouvrir une "nouvelle fenêtre" et une autre pour ouvrir une "nouvelle fenêtre privée".

Dans mon cas, la ligne à modifier est `Exec=firefox %u`
J'ai simplement modifié celle-ci en `Exec=firefox -private %u`

Même si je trouve que c'est un peu du bricolage, ça a le mérite de fonctionner. C'est magique cela s'applique automatique aux liens ouverts par d'autres applications. 
Par contre, l'inconvénient c'est qu'il n'est plus possible d'ouvrir une nouvelle fenêtre en mode "standard" ;-)

Je vais voir à l'usage si c'est une bonne idée !

