+++
author = "ValVin"
categories = ["blog", "divers"]
date = 2017-02-26T12:25:51Z
description = "Petit questionnement sur la possibilité de tenir un blog"
draft = false
featured_image = "/content/images/2017/02/computer-1295358_1280.png"
slug = "tenir-un-blog-ce-nest-pas-facile"
tags = ["blog", "divers"]
title = "Tenir un blog, ce n'est pas facile"

+++

Effectivement, il y a un peu plus d'un an, j'ai lancé ce blog. Même si je l'avais préssenti en le lançant, je ne suis pas assez assidu pour écrire un blog.

Ce n'est pas le manque de sujet à traiter, notamment sur la **vie-privée** qui me tient particulièrement à coeur ou même les **logiciels libres** en général. La vraie raison est que cela demande du temps et ma vie professionnelle et privée ne m'en accorde que le soir après le repas ... 

J'ai commencé à plusieurs reprises des billets que je n'ai jamais publié car arrêté en cours de route. Il y en a un sur Android en entreprise ou encore le lancement d'un Waze like Open-source qui aujourd'hui est la seule application pour laquelle je n'ai pas trouvé d'alternative. Qui lui même a généré un brouillon sur pgRouting ... bref, plein de commencement sans arrivée.

J'ai envie de faire des articles pour partager ce que je découvre / comprend avec mes mots. Mais avec les articles sur les [entrées DNS pour la messagerie](https://blog.valvin.fr/tag/smtp/), j'ai pu constater qu'il fallait consacrer une énergie considérable. Certainement la même énergie que doit consacrer un professeur lorsqu'il prépare un cours. Même si c'est clair dans sa tête, ce n'est pas si facile à mettre sur le papier.

Du coup, je me demande si je dois continuer à essayer ou tout simplement me dire que je ferais ça plus tard. J'avoue que des articles comme celui sur [Android sans google](https://blog.valvin.fr/2015/11/05/android-sans-google-1/) est assez motivant vu le nombre de visite que j'ai eu l'occasion d'avoir.  

D'autant que grâce à [Pepper&Carrot](http://www.peppercarrot.com), je me suis mis à dessiner avec pour objectif de lier mon loisir dessin avec mon côté militant pour la vie-privée... mais est ce que le résultat sera le même que pour ce blog ? 

Bref, il faudrait inventer un système qui permette d'allonger le temps ... ou alors trouver une bonne organisation qui permette de transmettre tout en prenant du plaisir et sans y consacrer trop de temps... 

Peut être que grâce à la nouvelle méthode GTD avec [todo.txt](http://todotxt.com/) (découverte grâce à [Craig Maonley](http://decafbad.net/) de P&C) je vais arriver à faire quelque chose de bien ? (tiens je pourrais en faire un article ...) 

```
Crédits Photo : OpenClipart-Vector (Pixabay) CC0
```