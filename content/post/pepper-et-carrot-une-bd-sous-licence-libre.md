+++
author = "ValVin"
categories = ["opensource"]
date = 2016-09-06T09:36:31Z
description = ""
draft = false
featured_image = "/content/images/2016/09/pepper-carrot-fixing.png"
slug = "pepper-et-carrot-une-bd-sous-licence-libre"
tags = ["opensource"]
title = "Pepper et Carrot - une BD sous licence libre"

+++

J'aime beaucoup la BD, même si on ne peut pas dire que je suis un grand passionné, je me laisse facilement absorber par les dessins et les bulles. J'adorerais savoir en faire autant ... mais je crois que ce sera pour une autre vie.

Il y a très peu de temps, je découvrais la BD [Pepper et Carrot](http://www.peppercarrot.com) après la lecture d'un article très intéressant de [Calimaq sur SILEX](https://scinfolex.com/2016/08/30/pepper-et-carrot-une-bande-dessinee-open-source-publiee-chez-glenat/), concernant l'édition de ce Webcomics sous licence libre. 
  
![](http://www.peppercarrot.com/0_sources/0ther/press/low-res/2015-10-12_logo_by-David-Revoy.jpg)

**Pepper** est une jeune sorcière, **Carrot** son chat l'accompagne dans toutes ces avantures remplies d'humour.

Ce qu'il me fait plaisir dans ce projet, c'est la preuve qu'il est possible de faire une **bande-dessinée sous licence libre** avec possibilité de commercialisation et de réussir à s'y retrouver financièrement. Et cela dans un autre domaine que le logiciel libre pour lequel des modèles économiques rentables existent.

La licence choisie est la [Creative Common By](https://creativecommons.org/licenses/by/4.0/) qui permet sa réutilisation, sa modification mais aussi sa commercialisation sans accord préalable de l'auteur, un crédit est suffisant.
[David Revoy](http://www.davidrevoy.com/), l'auteur de ce projet l'explique bien avec ce dessin :
![](http://www.peppercarrot.com/0_sources/0ther/misc/low-res/2015-02-09_philosophy_04-open-source_by-David-Revoy.jpg)

Il souhaiterait amener à réfléchir sur le fonctionnement de l'industrie de la bande-dessinnée, comme il l'illustre sur son site :
![](http://www.peppercarrot.com/data/images/static/2015-02-09_philosophy_06-industry-change.jpg)

D'ailleurs cette illustration me parait incomplète car dans ce cas préci, tout le monde peut apporter sa pierre à l'édifice : traduction, scénario ...

De plus, l'éditeur [Glénat](http://www.glenatbd.com/actu/-interview-david-revoy-pepper-carrot-bd-open-source.htm) rentre tout de même en compte mais plutôt que s'intercaler entre l'artiste et les distributeurs, il a les mêmes droits que n'importe quelle personne qui souhaiterait utiliser le travail réalisé. De plus l'éditeur a joué le jeu car il rémunère l'auteur au travers de Patreon pour qu'il puisse continuer à produire son webcomic (mais aussi envisager peut être un Tome 2) et propose un tarif à 9,90€ qui est inférieur au prix habituel des BD traditionnelles.

D'ailleurs si vous souhaitez vous la version Glénat auprès de libraire indépendant du Nord-Pas-de-Calais, voici le lien du réseau [Libr'Aire](https://www.libr-aire.fr/livre/10009418-pepper-et-carrot-potions-d-envol-revoy-david-glenat)

Sinon tout est disponible sur le site de Pepper et Carrot, dont le premier épisode en Français est [ici](http://www.peppercarrot.com/fr/article234/potion-of-flight)

Pour aider financièrement ce projet, voici quelques liens:

* le [Patreon](https://www.patreon.com/davidrevoy) et le [Tipee](https://www.tipeee.com/pepper-carrot) de David Revoy
* la boutique sur [DeviantArt](http://deevad.deviantart.com/prints/)

Après comme la licence est permissive, libre à vous de réutiliser, monétiser le travail et si vous le souhaitez sponsoriser les prochains épisodes! :) 






