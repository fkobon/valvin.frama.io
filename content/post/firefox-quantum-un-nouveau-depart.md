---
title: "Firefox Quantum Un Nouveau Depart"
date: 2017-11-18T13:54:53+01:00
featured_image: "/content/images/2017/Firefox-Quantum-Logo.png"
tags: ["firefox", "dessin", "opensource"]
draft: false
---
On en attend parler depuis déjà de nombreuses semaines (voire des mois) mais ça y est ! Il est disponible à tous, le nouveau Firefox : **[Firefox Quantum](https://www.mozilla.org/fr/firefox/)** (Firefox 57). Tout comme le Phénix renaît de ses cendres, il se veut être un nouveau Firefox. 

![](/content/images/2017/firefox_quantum_screenshot.png)

Firefox Quantum a refait une très grosse partie de son navigateur afin de rattraper le retard accumeler ces dernières années. Ils ont notamment énormément travailler sur les performances afin de la rendre beaucoup plus rapide et moins gourmand en ressources. Pour cela, ils ont abandonné le moteur du navigateur, Gecko, pour Quantum, un nouveau produit maison. (Chez Chrome, ce moteur s'appelle Webkit).

Bien entendu, ce n'est pas tout, il y aussi un nouveau design qui se veut beaucoup plus moderne. Et pour terminer, et peut être là où tout n'est pas tout à fait rose, un nouveau système d'extension basé sur les WebExtensions. Il s'agit d'un gros changement pour les développeurs des extensions existantes, changement qui a été anticipé déjà depuis plusieurs mois / années. Mais il reste encore quelque reliquat.

Quoi qu'il en soit, l'objectif est de reprendre des parts de marché à son concurrent principal qui est Google Chrome. 

Pour ma part, je teste cette version depuis quelques semaines déjà avec la *Developper edition* et j'avoue que la première sensation a été : "Wahooou", j'ai un sentiment de rapidité dont je n'avais pas l'habitude. J'ai eu une mauvaise surprise concernant les extensions, [Keefox](https://addons.mozilla.org/fr/firefox/addon/keefox/) mais maintenant une mise à jour a été faite et semble être compatible (pas encore testé, je viens de le découvrir en écrivant le billet). 

Et visiblement, en lisant les commentaires ici et là depuis la sortie officielle de cette nouvelle version, le travail réalisé est bien accueilli et notamment d'anciens utilisateurs Firefox qui avait basculé sur Chrome.   

Cela m'a inspiré ce petit dessin humouristique :

![](/content/images/2017/20171117_chrome_ie_autour_dun_verre.jpeg)

 

