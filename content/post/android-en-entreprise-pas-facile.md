+++
author = "ValVin"
date = 2016-01-22T20:37:16Z
description = ""
draft = true
slug = "android-en-entreprise-pas-facile"
title = "Android en entreprise : pas facile"

+++

Comme beaucoup de mon le sait, Android est le système d'exploitation de Google pour smartphones et tablettes. Celui-ci a été créé pour les utilisateurs grands publics. Il est donc livré avec un eco-système qui apporte nombreuses fonctionnalités sur le terminal.
 
En tant qu'utilisateur grand public, nous effectuons toute la configuration de notre terminal manuellement : configuration du wifi, installation de nos applications préférées et leurs paramétrages (quand ce n'est pas celles de Google). 
Ces petites tâches sur un terminal est assez facile, on pourrait évaluer celles-ci à une demi-heure environ. 

Dans le cas où nous devons effectuer ces tâches sur quelques centaines, voir milliers de terminaux, ça devient plus compliquer. C'est pourquoi depuis bien longtemps on industrialise / automatise ce type de tâches.

Mais voilà, ce que l'on fait sur des postes de travail, serveurs (windows ou linux) ou même terminaux industriels (windows mobile / ce), peut-on le faire sur les termimnaux android.

Nous allons voir que ce n'est pas si évident qu'il n'y parait.

###Les droits administrateurs
Sous windows, on doit être administrateur local ou du domaine, sous linux, un accès root ou sudo qui va bien mais sous Android, il n'y a pas réellement de notions d'administrateur.

La notion existe sous deux formes différentes :

- administrateur du terminal 
- application système
