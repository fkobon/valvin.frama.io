+++
author = "ValVin"
categories = ["divers"]
date = 2015-10-06T13:13:50Z
description = ""
draft = false
featured_image = "/content/images/2015/10/startup-593327_1280.jpg"
slug = "allez-hop-on-y-va"
tags = ["divers"]
title = "Allez hop !? ..."

+++

###... On y va! 
C'est parti pour mon tout premier billet sur ce petit blog sans prétention basé sur [Ghost](https://ghost.org/). 

J'espère que ce premier billet sera suivi de nombreux autres et surtout que quelques visiteurs viendront les lire :)

A vrai dire, il y a maintenant 10 ans, j'avais eu cette même intention, partager mes petites trouvailles au travers d'un blog. A l'époque c'était Blogspot, je souhaitais discuter d'une technologie avec laquelle j'ai débutée : le développement C#. J'ai fait 4 billets à tout casser et donc forcément, je pense pouvoir faire mieux.

###Alors quels sujets vais-je aborder ? 
Ce ne sera pas du .NET C# ! Je suis désintoxiqué :)

Tout d'abord, les articles seront très certainement orientés technique. Il y aura des articles :


* Sur **Linux**, les trucs et astuces qui me sont fort utiles pour mon poste de travail ou mes serveurs
* Sur le **développement** web / système
* Sur les projets **OpenSource** que j'utilise ou que je trouve intéressant
* Sur la **mobilité** : Android, MDM, matériel ...
* La **vie privée**, sujet un peu moins technique, quoi que, et qui me tient particulièrement à coeur.
* Et bien sûr, des sujets divers qui n'auront probablement pas de lien avec tout ça :)

Donc, si je fais un sujet sur chaque thème, je ferais mieux que mon blog précédent !