---
title: "First post with Hugo"
date: 2017-11-05T13:04:01+01:00
draft: true
---

# My first post with Hugo

Hugo is static website generator like Jekyll or many others. Once the client installed and some simple commands it is easy to publish his site.

## Installation


To install Hugo on Ubuntu it is possible to it by differents means : 

* User Ubuntu repository : the version is very old and it is not compatible with the i've chosen (Cactus) but i think it is the same for other themes.

```
sudo apt install hugo
```

Here we get a 0.16 version :(

* via la nouvelle méthode de containérisation de package **snap** : 

```
sudo snap install hugo
```

ici on a la version 0.32 mais lorsqu'on lance le client on a le message suivant :

```
snap-confine has elevated permissions and is not confined but should be. Refusing to continue to avoid permission escalation attacks
```

Ce message semble être un bug connu, lié à la version 16.04 d'Ubuntu : voir [ici](https://bugs.launchpad.net/ubuntu/+source/snapd/+bug/1673247)

Pour ma première tentative avec Snap, cela aurait été un échec. Il parait que Flatpak c'est mieux mais je n'ai pas d'avis sur le sujet.

* à la main :

C'est la méthode qui a fonctionné pour moi. Je suis allé sur le Github de Hugo est j'ai télécharge le .deb et je l'ai installé avec `dpkg`

